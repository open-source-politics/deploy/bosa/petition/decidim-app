require "extends/initiative_model_extend" if defined?(Decidim::Initiatives)
require "extends/initiative_admin_form_extend" if defined?(Decidim::Initiatives)
require "extends/organization_appearance_form_extend"
require "extends/update_organization_appearance_extend"
require "extends/create_omniauth_registration_extend"
